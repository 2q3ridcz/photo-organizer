﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path

$HereFolder = Get-Item -Path $here
$InputFolder = Get-ChildItem -Path $HereFolder | ?{$_.Name -Eq "Input"}
$OutputFolder = Get-ChildItem -Path $HereFolder | ?{$_.Name -Eq "Output"}

Get-ChildItem -Path $InputFolder -File |
?{ $_.Name -Ne ".gitignore" } |
%{
    $Prefix = $_.LastWriteTime.ToString("yyyyMMdd-HHmmss") + "_"
    $Destination = $OutputFolder | Join-Path -ChildPath ($Prefix + $_.Name)
    Copy-Item -Path $_.FullName -Destination $Destination -PassThru
}

Read-Host -Prompt ("Fin! Press enter to exit...")
