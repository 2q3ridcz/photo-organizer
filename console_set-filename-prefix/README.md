# photo-organizer

## console_set-filename-prefix

入力ファイルのファイル名の先頭に、そのファイルの最終更新日を設定する。

必要が生じて急遽作成した簡易的なツール。  
ゆくゆくは console_photo-organizer に統合したい。

### Usage

1. 対象ファイルを Input フォルダに格納する
2. ./console_set-filename-prefix.ps1 を起動する。
3. ./Output の出力結果を確認する。
