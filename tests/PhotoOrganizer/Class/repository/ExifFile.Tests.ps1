﻿using module ..\..\..\..\src\PhotoOrganizer\Class\repository\ExifFile.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ExifFile" {
    Context "GetExifDict" {
        It "Returns exif dict" {
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/blank.jpg"
            $Repo = [ExifFile]::new($PicPath)

            # When
            $ExifItemDict = $Repo.GetExifDict()

            # Then
            $ExifItemDict.keys | Measure-Object | %{$_.Count} | Should Be 0
        }
    }

    Context "Getters" {
        $TestCases = @(
            @{ "TestCase" = "Model" }
            @{ "TestCase" = "DateTimeOriginal" }
        )
        It "Throws given picture with no <TestCase>" -TestCases $TestCases {
            param ($TestCase, $Expect)
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/blank.jpg"
            $Repo = [ExifFile]::new($PicPath)

            # When # Then
            {$Repo.("Get" + $TestCase)()} | Should Throw
        }
    }

    Context "UpdateExifDict" {
        $TestCases = @(
            @{ "TestCase" = 1; "ExifDict" = @{
                "UnsupportedName" = "UnsupportedValue"
            } }
            @{ "TestCase" = 2; "ExifDict" = @{
                "DateTimeOriginal" = [DateTime]"2022/01/02 03:04:05"
                "UnsupportedName" = "UnsupportedValue"
            } }
        )
        It "Throws when input contains unsupported property <TestCase>" -TestCases $TestCases {
            param ($TestCase, $ExifDict)
            # Given
            $FileBaseName = "Throws when input contains unsupported property $TestCase".Replace(" ", "-")
            $BasePicPath = $here | Join-Path -ChildPath "test-data/blank.jpg"
            $PicPath = "$TestDrive\$FileBaseName.jpg"
            Copy-Item -Path $BasePicPath -Destination $PicPath
            $Repo = [ExifFile]::new($PicPath)

            # When # Then
            {$Repo.UpdateExifDict($ExifDict)} | Should Throw
        }

        $TestCases = @(
            @{ "TestCase" = 1; "ExifDict" = @{
                "DateTimeOriginal" = [DateTime]"2022/01/02 03:04:05"
            } }
            @{ "TestCase" = 2; "ExifDict" = @{
                "Model" = "Nice camera"
                "DateTimeOriginal" = [DateTime]"2022/01/02 03:04:05"
            } }
        )
        It "Sets <TestCase> property item to jpg file" -TestCases $TestCases {
            param ($TestCase, $ExifDict)
            # Given
            $FileBaseName = "Sets $TestCase property item to jpg file".Replace(" ", "-")
            $BasePicPath = $here | Join-Path -ChildPath "test-data/blank.jpg"
            $PicPath = "$TestDrive\$FileBaseName.jpg"
            Copy-Item -Path $BasePicPath -Destination $PicPath
            $Repo = [ExifFile]::new($PicPath)
            $Repo.ExifFileIO.GetPropertyItems() | Measure-Object | %{$_.Count} | Should Be 2

            # When
            $Repo.UpdateExifDict($ExifDict)

            # Then
            $Repo.ExifFileIO.GetPropertyItems() | Measure-Object | %{$_.Count} | Should Be (2 + $TestCase)
            $ResultDict = $Repo.GetExifDict()
            $ResultDict.Keys | Measure-Object | %{$_.Count} | Should Be $TestCase
            foreach ($Name in $ResultDict.Keys) {
                $ResultDict[$Name] | Should Be $ExifDict[$Name]
            }
        }
    }

    Context "Setters" {
        $TestCases = @(
            @{ "TestCase" = "Model"; "Value" = "Nice camera" }
            @{ "TestCase" = "DateTimeOriginal"; "Value" = [DateTime]"2022/01/02 03:04:05" }
        )
        It "Sets <TestCase> to jpg file" -TestCases $TestCases {
            param ($TestCase, $Value)
            # Given
            $BasePicPath = $here | Join-Path -ChildPath "test-data/blank.jpg"
            $PicPath = "$TestDrive\Sets-$TestCase.jpg"
            Copy-Item -Path $BasePicPath -Destination $PicPath
            $Repo = [ExifFile]::new($PicPath)
            $Repo.ExifFileIO.GetPropertyItems() | Measure-Object | %{$_.Count} | Should Be 2

            # When
            $Repo.("Set" + $TestCase)($Value)

            # Then
            $Repo.ExifFileIO.GetPropertyItems() | Measure-Object | %{$_.Count} | Should Be 3
            $Repo.("Get" + $TestCase)() | Measure-Object | %{$_.Count} | Should Be 1
            $Repo.("Get" + $TestCase)() | Should Be $Value
        }

        $TestCases = @(
            @{ "TestCase" = "Model"; "Value" = "Nice camera" }
            @{ "TestCase" = "DateTimeOriginal"; "Value" = [DateTime]"2022/01/02 03:04:05" }
        )
        It "Cannot set <TestCase> to gif file (does not throw)" -TestCases $TestCases {
            param ($TestCase, $Value)
            # Given
            $BasePicPath = $here | Join-Path -ChildPath "test-data/blank.gif"
            $PicPath = "$TestDrive\Sets-$TestCase.gif"
            Copy-Item -Path $BasePicPath -Destination $PicPath
            $Repo = [ExifFile]::new($PicPath)
            $Repo.ExifFileIO.GetPropertyItems() | Measure-Object | %{$_.Count} | Should Be 5

            # When
            $Repo.("Set" + $TestCase)($Value)

            # Then
            $Repo.ExifFileIO.GetPropertyItems() | Measure-Object | %{$_.Count} | Should Be 5
            { $Repo.("Get" + $TestCase)() } | Should Throw
        }
    }
}
