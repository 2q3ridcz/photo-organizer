﻿using module ..\..\..\..\src\PhotoOrganizer\Class\repository\ExifFileIO.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ExifFileIO" {
    Context "GetPropertyItems" {
        It "Throws if file does not exist" {
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/not-exist.jpg"
            $Repo = [ExifFileIO]::new($PicPath)

            # When # Then
            { $Repo.GetPropertyItems() } | Should Throw
        }

        It "Returns properties from gif file" {
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/blank.gif"
            $Repo = [ExifFileIO]::new($PicPath)

            # When
            $PropertyItems = $Repo.GetPropertyItems()

            # Then
            $PropertyItems | Measure-Object | %{$_.Count} | Should Be 5
            $PropertyItems[0].Id | Should Be 20736
            $PropertyItems[1].Id | Should Be 20737
            $PropertyItems[2].Id | Should Be 20738
            $PropertyItems[3].Id | Should Be 20739
            $PropertyItems[4].Id | Should Be 20740
        }

        It "Returns exif properties of jpg file" {
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/blank.jpg"
            $Repo = [ExifFileIO]::new($PicPath)

            # When
            $PropertyItems = $Repo.GetPropertyItems()

            # Then
            $PropertyItems | Measure-Object | %{$_.Count} | Should Be 2
            $PropertyItems[0].Id | Should Be 20625
            $PropertyItems[1].Id | Should Be 20624
        }
    }

    Context "UpdatePropertyItems" {
        # UpdatePropertyItems is tested in ExifFile.Tests.ps1.
    }
}
