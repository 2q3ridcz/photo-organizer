﻿using module ..\..\..\..\src\PhotoOrganizer\Class\ExifEntity\ExifValueConverter.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ExifValueConverter" {
    Context "Constructor" {
        It "Throws when created" {
            # Given # When # Then
            {[ExifValueConverter]::new()} | Should Throw
        }
    }

    Context "Abstract methods not implemented" {
        BeforeAll {
            class DummyExifValueConverter : ExifValueConverter {
                DummyExifValueConverter() {}
            }
            $Converter = [DummyExifValueConverter]::new()
        }
        It "Throws when Decode is called" {
            # Given # When # Then
            {$Converter.Decode()} | Should Throw
        }
        It "Throws when Encode is called" {
            # Given # When # Then
            {$Converter.Encode()} | Should Throw
        }
    }
}
