﻿using module ..\..\..\..\src\PhotoOrganizer\Class\ExifEntity\ExifProperty.psm1
using module ..\..\..\..\src\PhotoOrganizer\Class\ExifEntity\ExifValueConverter.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ExifProperty" {
    BeforeAll {
        class ExifDummyValueConverter : ExifValueConverter {
            ExifDummyValueConverter() {}
        }
        $Converter = [ExifDummyValueConverter]::new()
    }

    Context "IdInHex" {
        It "Returns expected value" {
            # Given
            $ExifProperty = [ExifProperty]::new(36867, 2, 20, "DateTimeOriginal", $Converter)

            # When
            $IdInHex = $ExifProperty.IdInHex()

            # Then
            $IdInHex | Measure-Object | %{$_.Count} | Should Be 1
            $IdInHex | Should Be "9003"
        }
    }

    Context "LengthFromEncodedValue" {
        It "Returns fixed length when FixedLength is set" {
            # Given
            $ExifProperty = [ExifProperty]::new(36867, 2, 20, "DateTimeOriginal", $Converter)

            # When
            $Length = $ExifProperty.LengthFromEncodedValue([Byte[]]::new(5))

            # Then
            $Length | Measure-Object | %{$_.Count} | Should Be 1
            $Length | Should Be 20
        }

        It "Returns length of value bytes when FixedLength is not set" {
            # Given
            $ExifProperty = [ExifProperty]::new(36867, 2, $Null, "DateTimeOriginal", $Converter)

            # When
            $Length = $ExifProperty.LengthFromEncodedValue([Byte[]]::new(5))

            # Then
            $Length | Measure-Object | %{$_.Count} | Should Be 1
            $Length | Should Be 5
        }
    }
}
