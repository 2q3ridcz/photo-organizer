﻿using module ..\..\..\..\src\PhotoOrganizer\Class\ExifEntity\ExifPropertyList.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ExifPropertyList" {
    Context "SearchById" {
        $TestCases = @(
            @{ "Testcase" = "valid"; "Id" = 36867; "Expect" = 1 }
            @{ "Testcase" = "inalid"; "Id" = [Int32]::MaxValue; "Expect" = 0 }
        )
        It "Returns <Expect> items for <Testcase> Id" -TestCases $TestCases {
            param ($Id, $Expect)
            # Given
            $ExifPropertyList = [ExifPropertyList]::new()

            # When
            $ExifProperty = $ExifPropertyList.SearchById($Id)

            # Then
            $ExifProperty | Measure-Object | %{$_.Count} | Should Be $Expect
            If ($Expect -Ne 0) {
                $ExifProperty.Id | Should Be $Id
            }
        }
    }

    Context "SearchByName" {
        It "Returns expected values" {
            # Given
            $ExifPropertyList = [ExifPropertyList]::new()

            # When
            $ExifProperty = $ExifPropertyList.SearchByName("DateTimeOriginal")

            # Then
            $ExifProperty | Measure-Object | %{$_.Count} | Should Be 1
            $ExifProperty.Name | Should Be "DateTimeOriginal"
        }
    }
}
