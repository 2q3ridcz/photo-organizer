$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Get-Exif" {
    Context "Operation verification" {
        It "Throws when Path is null" {
            # Given # When # Then
            {Get-Exif -Path $Null} | Should Throw
        }

        It "Throws when Path is invalid" {
            # Given # When # Then
            {Get-Exif -Path "abc"} | Should Throw
        }

        It "Returns exif dict" {
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/model-and-datetimeoriginal.jpg"

            # When
            $ExifDict = Get-Exif -Path $PicPath

            # Then
            $ExifDict | Measure-Object | %{$_.Count} | Should Be 1
            $ExifDict["DateTimeOriginal"] | Should Be ([DateTime]"2022/01/02 03:04:05")
            $ExifDict["Model"] | Should Be "Nice camera"
        }
    }
}
