$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

function Copy-TestInputFile {
    [CmdletBinding()]
    param(
        [string] $BaseFilePath,
        [string] $FolderName
    )

    process {
        $BaseFile = Get-Item -Path $BaseFilePath
        $Destination = "$TestDrive\$FolderName"
        New-Item -Path $Destination -ItemType Directory -Force | Out-Null
        $PicFile = Copy-Item -Path $BaseFile.FullName -Destination $Destination -PassThru
        $PicFile | Write-Output
    }
}

Describe "Set-PicInfo" {
    Context "Abnormal process" {
        It "Throws when Path is null" {
            # Given # When # Then
            {Set-PicInfo -Path $Null} | Should Throw
        }
    }

    Context "Jpg file" {
        $TestCases = @(
            @{ "Testcase" = "Does not change jpg file when whatif"; "WhatIf" = $True }
            @{ "Testcase" = "Changes jpg file when without whatif"; "WhatIf" = $False }
        )
        It "<Testcase>" -TestCases $TestCases {
            param ($Testcase, $WhatIf)
            # Given
            $FuncParam = @{
                "FolderName" = $Testcase.Replace(" ", "-")
                "BaseFilePath" = $here | Join-Path -ChildPath "test-data/model-and-datetimeoriginal.jpg"
            }
            $PicFile = Copy-TestInputFile @FuncParam
            $PreviousModel = $PicFile.Directory.Name
            $PreviousLastWriteTime = $PicFile.LastWriteTime
            $PreviousExifDict = Get-Exif -Path $PicFile.FullName

            # When
            $FuncParam = @{
                "Path" = $PicFile.FullName
                "DateTimeOriginal" = [DateTime]"1989/01/02 03:04:05"
                "Model" = "abc"
                "WhatIf" = $WhatIf
            }
            $Result = Set-PicInfo @FuncParam

            # Then
            $Result | Should Be $Null
            $ExifDict = Get-Exif -Path $PicFile.FullName
            $PicFile = Get-Item -Path $PicFile.FullName
            If ($WhatIf) {
                $ExifDict["Model"] | Should Be $PreviousExifDict["Model"]
                $ExifDict["DateTimeOriginal"] | Should Be $PreviousExifDict["DateTimeOriginal"]
                $ExifDict["Model"] | Should Not Be $FuncParam["Model"]
                $ExifDict["DateTimeOriginal"] | Should Not Be $FuncParam["DateTimeOriginal"]
                $PicFile.Directory.Name | Should Be $PreviousModel
                $PicFile.LastWriteTime | Should Be $PreviousLastWriteTime
                $PicFile.Directory.Name | Should Not Be $FuncParam["Model"]
                $PicFile.LastWriteTime | Should Not Be $FuncParam["DateTimeOriginal"]
            } Else {
                $ExifDict["Model"] | Should Not Be $PreviousExifDict["Model"]
                $ExifDict["DateTimeOriginal"] | Should Not Be $PreviousExifDict["DateTimeOriginal"]
                $ExifDict["Model"] | Should Be $FuncParam["Model"]
                $ExifDict["DateTimeOriginal"] | Should Be $FuncParam["DateTimeOriginal"]
                $PicFile.Directory.Name | Should Be $PreviousModel
                # $PicFile.LastWriteTime | Should Be $PreviousLastWriteTime
                $PicFile.Directory.Name | Should Not Be $FuncParam["Model"]
                $PicFile.LastWriteTime | Should Not Be $FuncParam["DateTimeOriginal"]
            }
        }
    }

    Context "Non-jpg file" {
        $TestCases = @(
            @{ "Testcase" = "Does not change DateTimeOriginal when whatif"; "WhatIf" = $True }
            @{ "Testcase" = "Changes only DateTimeOriginal when without whatif"; "WhatIf" = $False }
        )
        It "<Testcase>" -TestCases $TestCases {
            param ($Testcase, $WhatIf)
            # Given
            $FuncParam = @{
                "FolderName" = $Testcase.Replace(" ", "-")
                "BaseFilePath" = $here | Join-Path -ChildPath "test-data/00010101-000000_textfile.txt"
            }
            $PicFile = Copy-TestInputFile @FuncParam

            $PreviousModel = $PicFile.Directory.Name
            $PreviousLastWriteTime = $PicFile.LastWriteTime

            # When
            $FuncParam = @{
                "Path" = $PicFile.FullName
                "DateTimeOriginal" = [DateTime]"1989/01/02 03:04:05"
                "Model" = "abc"
                "WhatIf" = $WhatIf
            }
            $Result = Set-PicInfo @FuncParam

            # Then
            $Result | Should Be $Null
            $PicFile = Get-Item -Path $PicFile.FullName
            If ($WhatIf) {
                $PicFile.Directory.Name | Should Be $PreviousModel
                $PicFile.LastWriteTime | Should Be $PreviousLastWriteTime
                $PicFile.Directory.Name | Should Not Be $FuncParam["Model"]
                $PicFile.LastWriteTime | Should Not Be $FuncParam["DateTimeOriginal"]
            } Else {
                $PicFile.Directory.Name | Should Be $PreviousModel
                $PicFile.LastWriteTime | Should Not Be $PreviousLastWriteTime
                $PicFile.Directory.Name | Should Not Be $FuncParam["Model"]
                $PicFile.LastWriteTime | Should Be $FuncParam["DateTimeOriginal"]
            }
        }
    }
}
