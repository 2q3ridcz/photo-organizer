$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Get-PicInfo" {
    Context "Abnormal process" {
        It "Throws when Path is null" {
            # Given # When # Then
            {Get-PicInfo -Path $Null} | Should Throw
        }

        It "Returns null when Path is invalid" {
            # Given # When # Then
            Get-PicInfo -Path "abc" | Should Be $Null
        }
    }

    Context "Jpg file" {
            $TestCases = @(
            @{ "Testcase" = "from directory"; "SetModelFromDirectory" = $True; "Expect" = "test-data" }
            @{ "Testcase" = "'Unknown'"; "SetModelFromDirectory" = $False; "Expect" = "Nice camera" }
        )
        It "Returns datetime from exif and model <Testcase>" -TestCases $TestCases {
            param ($SetModelFromDirectory, $Expect)
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/model-and-datetimeoriginal.jpg"

            # When
            $FuncParam = @{
                "Path" = $PicPath
                "SetModelFromDirectory" = $SetModelFromDirectory
            }
            $PicData = Get-PicInfo @FuncParam

            # Then
            $PicData | Measure-Object | %{$_.Count} | Should Be 1
            $PicData.DateTimeOriginal | Should Be ([DateTime]"2022/01/02 03:04:05")
            $PicData.Model | Should Be $Expect
        }

        $TestCases = @(
            @{ "Testcase" = "from directory"; "SetModelFromDirectory" = $True; "Expect" = "test-data" }
            @{ "Testcase" = "'Unknown'"; "SetModelFromDirectory" = $False; "Expect" = "Unknown" }
        )
        It "Returns datetime from filename and model <Testcase>" -TestCases $TestCases {
            param ($SetModelFromDirectory, $Expect)
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/20221001-143001_blank.jpg"

            # When
            $FuncParam = @{
                "Path" = $PicPath
                "SetModelFromDirectory" = $SetModelFromDirectory
            }
            $PicData = Get-PicInfo @FuncParam

            # Then
            $PicData | Measure-Object | %{$_.Count} | Should Be 1
            $PicData.DateTimeOriginal | Should Be ([DateTime]"2022/10/01 14:30:01")
            $PicData.Model | Should Be $Expect
        }
    }

    Context "Non jpg file" {
        $TestCases = @(
            @{ "Testcase" = "from directory"; "SetModelFromDirectory" = $True; "Expect" = "test-data" }
            @{ "Testcase" = "'Unknown'"; "SetModelFromDirectory" = $False; "Expect" = "Unknown" }
        )
        It "Returns datetime from filename and model <Testcase>" -TestCases $TestCases {
            param ($SetModelFromDirectory, $Expect)
            # Given
            $PicPath = $here | Join-Path -ChildPath "test-data/00010101-000000_textfile.txt"

            # When
            $FuncParam = @{
                "Path" = $PicPath
                "SetModelFromDirectory" = $SetModelFromDirectory
            }
            $PicData = Get-PicInfo @FuncParam

            # Then
            $PicData | Measure-Object | %{$_.Count} | Should Be 1
            $PicData.DateTimeOriginal | Should Be ([DateTime]"0001/01/01 00:00:00")
            $PicData.Model | Should Be $Expect
        }
    }
}
