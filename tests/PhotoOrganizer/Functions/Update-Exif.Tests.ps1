$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Update-Exif" {
    Context "Operation verification" {
        It "Throws when Path is null" {
            # Given # When # Then
            {Update-Exif -Path $Null} | Should Throw
        }

        It "Sets exif" {
            # Given
            $FileBaseName = "Sets exif".Replace(" ", "-")
            $BasePicPath = $here | Join-Path -ChildPath "test-data/model-and-datetimeoriginal.jpg"
            $PicPath = "$TestDrive\$FileBaseName.jpg"
            Copy-Item -Path $BasePicPath -Destination $PicPath

            $ExifDict = @{
                "Model" = "Nice camera"
                "DateTimeOriginal" = [DateTime]"2022/01/02 03:04:05"
            }

            # When
            Update-Exif -Path $PicPath -ExifDict $ExifDict

            # Then
            $ResultDict = Get-Exif -Path $PicPath
            $ResultDict | Measure-Object | %{$_.Count} | Should Be 1
            $ResultDict["DateTimeOriginal"] | Should Be ([DateTime]"2022/01/02 03:04:05")
            $ResultDict["Model"] | Should Be "Nice camera"
        }
    }
}
