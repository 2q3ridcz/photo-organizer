﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$FunctionsFolderPath = "$here\Functions" | Resolve-Path

Get-ChildItem -Path $FunctionsFolderPath |
?{ $_.Extension -Eq ".psm1" } |
%{
    Import-Module -Name $_.FullName -Force
    Export-ModuleMember -Function $_.BaseName
}
