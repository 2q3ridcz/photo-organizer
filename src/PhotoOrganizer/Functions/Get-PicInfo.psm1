﻿<#
.SYNOPSIS
    Creates picture info from a file
.DESCRIPTION
    Gets DateTimeOriginal and Model from a file.

    Gets DateTimeOriginal from one of the following:
    1. Exif of the file
    2. Name of the file
        1. Datetime in "yyyyMMdd-HHmmss" format at the start of the filename

    Gets Model from one of the following:
    1. Name of the directory, if SetModelFromDirectory argument is set.
    2. Exif of the file
    3. 'Unknown' (fixed string)
.EXAMPLE
    Get-PicInfo -Path "C:\model\pic.jpg" -SetModelFromDirectory
    Returns a PSObject having DateTimeOriginal and Model properties.
#>
function Get-PicInfo {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSReviewUnusedParameter", "")]
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string] $Path
        ,
        [Parameter(Mandatory=$False, ValueFromPipeline=$False, Position=1)]
        [switch] $SetModelFromDirectory
    )
    begin {}
    process {
        Get-Item -Path $Path |
        %{
            $File = $_
            $Datetime = $Null
            $Model = $Null
            If (@(".jpg", ".jpeg") -Contains $File.Extension) {
                # Exif設定可能の場合
                $ExifDict = Get-Exif -Path $File.FullName
                $Datetime = $ExifDict["DateTimeOriginal"]
                $Model = $ExifDict["Model"]
            }

            If ($Null -Eq $Datetime) {
                # Exif設定可能の場合 かつ Datetime未設定の場合
                # Exif設定不可能の場合
                # ファイル名の先頭から Datetimeを抽出する
                $DatetimeStr = $File.Name.Substring(0, 15)
                $Datetime = [Datetime]::ParseExact($DatetimeStr, "yyyyMMdd-HHmmss", $Null)
            }

            If ($SetModelFromDirectory) {
                $Model = $File.Directory.Name
            }
            If ($Null -Eq $Model) {
                $Model = "Unknown"
            }

            New-Object -TypeName psobject -Property @{
                "DateTimeOriginal" = $Datetime
                "Model" = $Model
            } | Write-Output
        }
    }
    end {}
}
