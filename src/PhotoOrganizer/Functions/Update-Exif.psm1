﻿using module ..\Class\repository\ExifFile.psm1

function Update-Exif {
    [CmdletBinding(SupportsShouldProcess = $true)]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$True, ValueFromPipeline=$False, Position=1)]
        [Hashtable]
        $ExifDict
    )
    begin {}
    process {
        $Repo = [ExifFile]::new($Path)
        $FileName = Split-Path -Path $Path -Leaf

        If ($PSCmdlet.ShouldProcess($FileName)) {
            $Repo.UpdateExifDict($ExifDict)
        }
    }
    end {}
}
