﻿<#
.SYNOPSIS
    Sets picture info to a file
.DESCRIPTION
    Sets DateTimeOriginal and Model to a file.

    - For jpg files
        - Sets DateTimeOriginal and Model to exif.
    - For non-jpg files:
        - Sets DateTimeOriginal to LastWriteTime.
        - Do not set Model.
.EXAMPLE
    Set-PicInfo -Path "C:\model\pic.jpg" -DateTimeOriginal (Get-Date) -Model "model"
    Sets DateTimeOriginal and Model to the file's exif.
.EXAMPLE
    Set-PicInfo -Path "C:\model\pic.gif" -DateTimeOriginal (Get-Date) -Model "model"
    Sets DateTimeOriginal to a file's LastWriteTime. Value "model" will be ommited.
#>
function Set-PicInfo {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSReviewUnusedParameter", "")]
    [CmdletBinding(SupportsShouldProcess = $true)]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$True, ValueFromPipeline=$False, Position=1)]
        [Datetime]
        $DateTimeOriginal
        ,
        [Parameter(Mandatory=$True, ValueFromPipeline=$False, Position=2)]
        [string]
        $Model
    )
    begin {}
    process {
        Get-Item -Path $Path |
        %{
            $File = $_
            If (@(".jpg", ".jpeg") -Contains $File.Extension) {
                # Exif設定可能の場合
                $ExifDict = @{}
                $CurrentExifDict = Get-Exif -Path $File.FullName
                If ($DateTimeOriginal -Ne $CurrentExifDict["DateTimeOriginal"]) {
                    $ExifDict["DateTimeOriginal"] = $DateTimeOriginal
                }
                If ($Model -Ne $CurrentExifDict["Model"]) {
                    $ExifDict["Model"] = $Model
                }
                Update-Exif -Path $File.FullName -ExifDict $ExifDict -WhatIf:$WhatIfPreference
            } Else {
                # $CurrentDatetimeStr = $File.Name.Substring(0, 15)
                # $CurrentDatetime = [Datetime]::ParseExact($CurrentDatetimeStr, "yyyyMMdd-HHmmss", $Null)
                # If ($DateTimeOriginal -Ne $CurrentDatetime) {
                #     $DatetimeStr = $DateTimeOriginal.ToSTring("yyyyMMdd-HHmmss")
                #     $FileName = $DatetimeStr + "_" + $File.Name
                #     Rename-Item -Path $File.FullName -NewName $FileName
                # }
                If ($File.LastWriteTime -Ne $DateTimeOriginal) {
                    If ($PSCmdlet.ShouldProcess($File.Name, "set LastWriteTime to $DateTimeOriginal")) {
                        $File.LastWriteTime = $DateTimeOriginal
                    }
                }
            }
        }
    }
    end {}
}
