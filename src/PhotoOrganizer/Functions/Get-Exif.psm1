﻿using module ..\Class\repository\ExifFile.psm1

function Get-Exif {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string]
        $Path
    )
    begin {}
    process {
        $Repo = [ExifFile]::new($Path)
        $ExifItemDict = $Repo.GetExifDict()
        $ExifItemDict | Write-Output
    }
    end {}
}
