﻿using module ..\ExifEntity\ExifPropertyList.psm1
using module ..\entity\ExifPropertyItem.psm1
using module ..\entity\ExifPropertyItemList.psm1
using module .\ExifFileIO.psm1

class ExifFile {
    [ExifFileIO] $ExifFileIO

    ExifFile(
        [string] $Path
    ) {
        [Reflection.Assembly]::LoadWithPartialName("System.Drawing") | Out-Null
        $this.ExifFileIO = [ExifFileIO]::new($Path)
    }

    [void] UpdateExifDict([Hashtable] $ExifDict) {
        # Sets the property items from a dict.
        # A dict where name is the key and decoded value is the value.
        # Only sets properties defined in ExifPropertyList.
        # If the dict contains those not defined, it throws.
        $ExifPropertyList = [ExifPropertyList]::new()

        $ExifPropertyItemList = [ExifPropertyItemList]::new()
        foreach ($Name in $ExifDict.keys) {
            $ExifProperty = $ExifPropertyList.SearchByName($Name)

            If ($Null -Ne $ExifProperty) {
                $Value = $ExifProperty.EncodeValue($ExifDict[$Name])
                $Length = $ExifProperty.LengthFromEncodedValue($Value)

                $ExifPropertyItemList.Add(
                    [ExifPropertyItem]::new(
                        $ExifProperty.Id,
                        $Length,
                        $ExifProperty.Type,
                        $Value
                    )
                )
            } Else {
                Throw "Property '$Name' is not supported"
            }
        }
        $this.ExifFileIO.UpdatePropertyItems($ExifPropertyItemList)
    }

    [Hashtable] GetExifDict() {
        # Returns the property items as a dict,
        # where name is the key and decoded value is the value.
        # Only returns properties defined in ExifPropertyList.
        # Those not defined are omitted.
        $ExifPropertyItemList = $this.ExifFileIO.GetPropertyItems()

        $Result = @{}
        $ExifPropertyList = [ExifPropertyList]::new()
        foreach ($PropertyItem in $ExifPropertyItemList) {
            $ExifProperty = $ExifPropertyList.SearchById($PropertyItem.Id)
            If ($Null -Ne $ExifProperty) {
                $Value = $ExifProperty.DecodeValue($PropertyItem.Value)
                $Result[$ExifProperty.Name] = $Value
            }
        }
        Return $Result
    }

    [object] GetValueByName([string] $Name) {
        $ExifItemDict = $this.GetExifDict()
        If (-Not $ExifItemDict.ContainsKey($Name)) {
            Throw "$Name does not exist."
        }
        Return $ExifItemDict[$Name]
    }

    [string] GetModel() { Return $this.GetValueByName("Model") }
    [void] SetModel([object] $Value) { $this.UpdateExifDict(@{"Model" = $Value}) }

    [DateTime] GetDateTimeOriginal() { Return $this.GetValueByName("DateTimeOriginal") }
    [void] SetDateTimeOriginal([DateTime] $DateTime) { $this.UpdateExifDict(@{"DateTimeOriginal" = $DateTime}) }
}
