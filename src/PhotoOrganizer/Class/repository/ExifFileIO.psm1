﻿using module ..\entity\ExifPropertyItem.psm1
using module ..\entity\ExifPropertyItemList.psm1

class ExifFileIO {
    [string] $Path

    ExifFileIO(
        [string] $Path
    ) {
        [Reflection.Assembly]::LoadWithPartialName("System.Drawing") | Out-Null
        $this.Path = $Path
    }

    [ExifPropertyItemList] GetPropertyItems() {
        $PicPath = $this.Path
        $Bitmap = $Null
        $PropertyItems = @()
        try {
            $Bitmap = New-Object Drawing.Bitmap($PicPath)
            $PropertyItems = $Bitmap.PropertyItems
        } finally {
            If ($Null -Ne $Bitmap) {
                $Bitmap.Dispose()
                $Bitmap = $Null
            }
            [GC]::Collect()
        }
        $ExifPropertyItemList = [ExifPropertyItemList]::new()
        foreach ($PropertyItem in $PropertyItems) {
            $ExifPropertyItemList.Add(
                [ExifPropertyItem]::new(
                    $PropertyItem.Id,
                    $PropertyItem.Len,
                    $PropertyItem.Type,
                    $PropertyItem.Value
                )
            )
        }
        Return $ExifPropertyItemList
    }

    [void] UpdatePropertyItems(
        [ExifPropertyItemList] $ExifPropertyItemList
    ) {
        $PicPath = $this.Path

        $Bitmap = $Null
        $TemporaryFile = New-TemporaryFile
        try {
            try {
                $Bitmap = New-Object Drawing.Bitmap($PicPath)

                foreach ($ExifPropertyItem in $ExifPropertyItemList) {
                    $PropertyItem = $Bitmap.PropertyItems[0]
                    $PropertyItem.Id = $ExifPropertyItem.Id
                    $PropertyItem.Len = $ExifPropertyItem.Len
                    $PropertyItem.Type = $ExifPropertyItem.Type
                    $PropertyItem.Value = $ExifPropertyItem.Value

                    $Bitmap.SetPropertyItem($PropertyItem)
                }
                # Saveメソッドで直接上書きしようとすると "GDI+ で汎用エラーが発生" する。
                # そのためここでは一時ファイルに出力し、
                # Bitmapを Disposeした後で元のファイルに上書きコピーする。
                $Bitmap.Save($TemporaryFile.FullName)
            } finally {
                If ($Null -Ne $Bitmap) {
                    $Bitmap.Dispose()
                    $Bitmap = $Null
                }
                [GC]::Collect()
            }

            Copy-Item -Path $TemporaryFile -Destination $PicPath
        } finally {
            If ($Null -Ne $TemporaryFile) {
                If ($TemporaryFile.Exists) {
                    Remove-Item $TemporaryFile.FullName -Force
                }
            }
            [GC]::Collect()
        }
    }
}
