﻿# Thanks to this page for how to code abstract classes.
# https://xainey.github.io/2016/powershell-classes-and-concepts/#abstract-classes
class ExifValueConverter {
    ExifValueConverter () {
        $type = $this.GetType()
        if ($type -eq [ExifValueConverter]) {
            throw("Class $type must be inherited")
        }
    }
    [object] Decode() { throw("Must Override Method") }
    [Byte[]] Encode() { throw("Must Override Method") }
}
