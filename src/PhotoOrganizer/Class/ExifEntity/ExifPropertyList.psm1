﻿using module .\ExifProperty.psm1
using module .\ExifValueConverterImpl\ExifDatetimeValueConverter.psm1
using module .\ExifValueConverterImpl\ExifStringValueConverter.psm1

class ExifPropertyList : System.Collections.Generic.List[ExifProperty] {
    # https://learn.microsoft.com/ja-jp/dotnet/api/system.drawing.imaging.propertyitem.id
    # https://exiftool.org/TagNames/EXIF.html
    # Convert Hex to int like this: [Int32]0x9003
    ExifPropertyList() {
        @(
            [ExifProperty]::new(272, 2, $Null, "Model", [ExifStringValueConverter]::new()),
            [ExifProperty]::new(36867, 2, 20, "DateTimeOriginal", [ExifDatetimeValueConverter]::new())
        ) |
        %{ $this.Add($_) }
    }

    [ExifProperty] SearchById([Int32] $Id) {
        $Result = $this | ?{$_.Id -Eq $Id}
        Return $Result
    }

    [ExifProperty] SearchByName([string] $Name) {
        $Result = $this | ?{$_.Name -Eq $Name}
        Return $Result
    }
}
