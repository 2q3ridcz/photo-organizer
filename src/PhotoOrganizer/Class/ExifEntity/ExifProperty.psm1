﻿using module .\ExifValueConverter.psm1

class ExifProperty {
    [Int32] $Id
    [Int16] $Type
    [Nullable[Int32]] $FixedLength
    [string] $Name
    [ExifValueConverter] $Converter

    ExifProperty(
        [Int32] $Id,
        [Int16] $Type,
        [Nullable[Int32]] $FixedLength,
        [string] $Name,
        [ExifValueConverter] $Converter
    ) {
        $this.Id = $Id
        $this.Type = $Type
        $this.FixedLength = $FixedLength
        $this.Name = $Name
        $this.Converter = $Converter
    }

    [string] IdInHex() {
        Return '{0:X}' -f $this.Id
    }

    [Int32] LengthFromEncodedValue([Byte[]] $Bytes) {
        If ($Null -Ne $this.FixedLength) {
            Return $this.FixedLength
        } Else {
            Return $Bytes.Length
        }
    }

    [object] DecodeValue([Byte[]] $Bytes) {
        Return $this.Converter.Decode($Bytes)
    }

    [Byte[]] EncodeValue([object] $Value) {
        Return $this.Converter.Encode($Value)
    }
}
