﻿using module ..\ExifValueConverter.psm1

class ExifDatetimeValueConverter : ExifValueConverter {
    # フォーマットは「YYYY:MM:DD HH:MM:SS」で、0～24時表記、日付と時間の間に空白文字を1つ埋める。
    # https://knowledge.moshimore.jp/entry/exif_datetime_subsec_offset/#i-4
    ExifDatetimeValueConverter () {}
    [DateTime] Decode([Byte[]] $Value) {
        $DateTimeStr = [System.Text.Encoding]::UTF8.GetString($Value)
        Return [DateTime]::ParseExact($DateTimeStr.Substring(0, 19), "yyyy:MM:dd HH:mm:ss", $Null)
    }

    [Byte[]] Encode([DateTime] $Datetime) {
        Return [System.Text.Encoding]::UTF8.getBytes( $DateTime.ToString("yyyy:MM:dd HH:mm:ss") ) + [Byte[]]0
    }
}
