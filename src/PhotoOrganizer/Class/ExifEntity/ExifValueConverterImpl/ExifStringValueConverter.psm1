﻿using module ..\ExifValueConverter.psm1

class ExifStringValueConverter : ExifValueConverter {
    ExifStringValueConverter () {}
    [string] Decode([Byte[]] $Value) {
        $String = [System.Text.Encoding]::UTF8.GetString($Value[0..($Value.Length-2)])
        Return $String
    }

    [Byte[]] Encode([string] $String) {
        Return [System.Text.Encoding]::UTF8.getBytes($String) + [Byte[]]0
    }
}
