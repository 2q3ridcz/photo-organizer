﻿class ExifPropertyItem {
    [Int32] $Id
    [Int32] $Len
    [Int16] $Type
    [Byte[]] $Value

    ExifPropertyItem(
        [Int32] $Id,
        [Int32] $Len,
        [Int16] $Type,
        [Byte[]] $Value
    ) {
        $this.Id = $Id
        $this.Len = $Len
        $this.Type = $Type
        $this.Value = $Value
    }
}