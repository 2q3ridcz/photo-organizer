# photo-organizer

Tools for organizing my photos.

## class-diagram

```mermaid
flowchart LR
subgraph class-diagram
    subgraph console
        console_photo-organizer
    end
    subgraph PhotoOrganizer
        subgraph Functions
            Get-PicInfo["Get-PicInfo"]
            %% Copy-ItemToPicFolder["Copy-ItemToPicFolder"]
            Set-PicInfo["Set-PicInfo"]
            Get-Exif["Get-Exif"]
            Update-Exif["Update-Exif"]
        end
        subgraph Class
            subgraph repository
                ExifFile["ExifFile<br/>---<br/>GetExifDict()<br/>UpdateExifDict($ExifItemList)"]
                ExifFileIO["ExifFileIO<br/>---<br/>GetPropertyItems()<br/>UpdatePropertyItems($ExifPropertyItemList)"]
            end
            subgraph ExifEntity
                ExifProperty["ExifProperty"]
                ExifPropertyList["ExifPropertyList"]
                ExifValueConverter["ExifValueConverter"]
                subgraph ExifValueConverterImpl
                    ExifDatetimeValueConverter["ExifDatetimeValueConverter"]
                end
            end
            subgraph entity
                ExifPropertyItem["ExifPropertyItem"]
                ExifPropertyItemList["ExifPropertyItemList"]
            end
        end
    end
end
console_photo-organizer --> Get-PicInfo
%% console_photo-organizer --> Copy-ItemToPicFolder
console_photo-organizer --> Set-PicInfo
Get-PicInfo --> Get-Exif
Set-PicInfo --> Get-Exif
Set-PicInfo --> Update-Exif
Get-Exif --> ExifFile
Update-Exif --> ExifFile
ExifPropertyItemList --> ExifPropertyItem
ExifProperty --> ExifValueConverter
ExifPropertyList --> ExifProperty
ExifPropertyList --> ExifDatetimeValueConverter
ExifDatetimeValueConverter --> ExifValueConverter
ExifFile --> ExifPropertyList
ExifFile --> ExifPropertyItem
ExifFile --> ExifPropertyItemList
ExifFile --> ExifFileIO
ExifFileIO --> ExifPropertyItem
ExifFileIO --> ExifPropertyItemList
```
