﻿# ★未完成★
# 大まかな流れは、以下の通り。
# 1. ファイルやパスから Datetimeと Modelを特定する
# 2. Datetimeと Modelに応じた出力先へコピーする
#     - ./Output/[yyyymm]99_その他/[Model]/[ファイル名]
# 3. ファイルの内容に応じてファイルを編集する
#     - Exif設定可能の場合、以下を実施。
#         - Exifに Datetimeと Modelが設定済みの場合、何もしない。
#         - Exifに Datetimeや Modelが未設定の場合、設定する。
#     - Exif設定不可能の場合、modifiedtimeに Datetimeを設定し、さらに以下を実施。
#         - ファイル名から Datetimeを判別可能の場合、何もしない。
#         - ファイル名から Datetimeを判別不可能の場合、ファイル名先頭に Datetimeを設定する。
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$PackageName = "PhotoOrganizer"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force

$HereFolder = Get-Item -Path $here
$InputFolder = Get-ChildItem -Path $HereFolder | ?{$_.Name -Eq "Input"}
$OutputFolder = Get-ChildItem -Path $HereFolder | ?{$_.Name -Eq "Output"}

function Copy-ItemToPicFolder (
    [string] $Path,
    [string] $DestinationRoot,
    [Datetime] $Datetime,
    [string] $Model
) {
    $ChildPath = $Datetime.ToString("yyyyMM") + "99_その他/" + $Model
    $DestFolderPath = $DestinationRoot | Join-Path -ChildPath $ChildPath
    $DestFolder = New-Item -Path $DestFolderPath -ItemType Directory -Force

    $CopyFile = Copy-Item -Path $Path -Destination $DestFolder -PassThru
    $CopyFile | Write-Output
}


# Exif の撮影機器を使用したい場合
Get-ChildItem -Path $InputFolder |
?{$_.Name -Eq "ModelExifIsSet"} |
Get-ChildItem |
?{$_.Name -Ne ".gitignore"} |
%{
    $File = $_
    $PicData = Get-PicInfo -Path $File.FullName

    $FuncParam = @{
        "Path" = $File.FullName
        "DestinationRoot" = $OutputFolder.FullName
        "Datetime" = $PicData.DateTimeOriginal
        "Model" = $PicData.Model
    }
    $CopyFile = Copy-ItemToPicFolder @FuncParam

    $FuncParam = @{
        "Path" = $CopyFile.FullName
        "DateTimeOriginal" = $PicData.DateTimeOriginal
        "Model" = $PicData.Model
    }
    Set-PicInfo @FuncParam
    $CopyFile
}

# 撮影機器を指定したい場合
Get-ChildItem -Path $InputFolder |
?{$_.Name -Eq "ModelExifIsNotSet"} |
Get-ChildItem |
?{$_.Name -Ne ".gitignore"} |
Get-ChildItem |
%{
    $File = $_
    $PicData = Get-PicInfo -Path $File.FullName -SetModelFromDirectory

    $FuncParam = @{
        "Path" = $File.FullName
        "DestinationRoot" = $OutputFolder.FullName
        "Datetime" = $PicData.DateTimeOriginal
        "Model" = $PicData.Model
    }
    $CopyFile = Copy-ItemToPicFolder @FuncParam

    $FuncParam = @{
        "Path" = $CopyFile.FullName
        "DateTimeOriginal" = $PicData.DateTimeOriginal
        "Model" = $PicData.Model
    }
    Set-PicInfo @FuncParam
    $CopyFile
}

Read-Host -Prompt ("Fin! Press enter to exit...")
